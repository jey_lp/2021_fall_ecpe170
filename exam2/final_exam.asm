.globl main
.text

.LC1:
        .ascii  "Search key not found\000"
.LC2:
        .ascii  "Element %i found at array index %i\012\000"
.LC0:
        .word   2
        .word   3
        .word   5
        .word   7
        .word   11
main:
	#Register Map
	# $1
	# $2
	# $3
	# $4
	# $5
	# $6
	# $7
	# $25
	# $28
	
	
        li      $2,5                        # 0x5
        sw      $2,0($fp)
        li      $2,7                        # 0x7
        sw      $2,4($fp)
        lw      $2,0($fp)
        addiu   $2,$2,-1
        
        move    $3,$2
        daddiu  $sp,$sp,-80
        sd      $31,72($sp)
        sd      $fp,64($sp)
        sd      $28,56($sp)
        move    $fp,$sp

        lui     $28,%hi(%neg(%gp_rel(main)))
        daddu   $28,$28,$25
        daddiu  $28,$28,%lo(%neg(%gp_rel(main)))
        ld      $2,%got_page(.LC0)($28)
        daddiu  $3,$2,%got_ofst(.LC0)
        ldl     $3,7($3)
        ldr     $3,%got_ofst(.LC0)($2)
        move    $4,$3
        daddiu  $3,$2,%got_ofst(.LC0)
        ldl     $5,15($3)
        ldr     $5,8($3)
        move    $3,$5
        sd      $4,16($fp)
        sd      $3,24($fp)
        daddiu  $2,$2,%got_ofst(.LC0)
        lw      $2,16($2)
        sw      $2,32($fp)
        lw      $4,4($fp)
        daddiu  $2,$fp,16
        move    $6,$4
        move    $5,$3
        move    $4,$2
        ld      $2,%got_disp(arraySearch)($28)
        move    $25,$2
        nop

        sw      $2,8($fp)
        lw      $2,8($fp)
        bgez    $2,.L2
        nop

        ld      $2,%got_page(.LC1)($28)
        daddiu  $4,$2,%got_ofst(.LC1)
        ld      $2,%call16(puts)($28)
        move    $25,$2
        nop

        b       .L3
        nop

.L2:
        lw      $3,8($fp)
        lw      $2,4($fp)
        move    $6,$3
        move    $5,$2
        ld      $2,%got_page(.LC2)($28)
        daddiu  $4,$2,%got_ofst(.LC2)
        ld      $2,%call16(printf)($28)
        move    $25,$2
        nop

.L3:
        move    $2,$0
        move    $sp,$fp
        ld      $31,72($sp)
        ld      $fp,64($sp)
        ld      $28,56($sp)

arraySearch:
	#Register Map
	

        daddiu  $sp,$sp,-64
        sd      $31,56($sp)
        sd      $fp,48($sp)
        sd      $28,40($sp)
        
        sw      $2,28($fp)
        lw      $2,24($fp)
        dsll    $2,$2,2
        
        move    $fp,$sp
        daddu   $28,$28,$25
        daddiu  $28,$28,%lo(%neg(%gp_rel(arraySearch)))
        sd      $4,16($fp)
        move    $3,$5
        move    $2,$6
        sll     $3,$3,0
        sw      $3,24($fp)
        sll     $2,$2,0
        
        ld      $3,16($fp)
        daddu   $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        bne     $2,$3,.L6
        nop

        lw      $2,24($fp)
        sw      $2,0($fp)
        b       .L7
        nop

.L6:
        lw      $3,24($fp)
        li      $2,-1                 # 0xffffffffffffffff
        bne     $3,$2,.L8
        nop

        li      $2,-1                 # 0xffffffffffffffff
        sw      $2,0($fp)
        b       .L7
        nop

.L8:
        lw      $2,24($fp)
        addiu   $2,$2,-1
        lw      $3,28($fp)
        move    $6,$3
        move    $5,$2
        ld      $4,16($fp)
        ld      $2,%got_disp(arraySearch)($28)
        move    $25,$2
        nop

        sw      $2,0($fp)
.L7:
        lw      $2,0($fp)
        move    $sp,$fp
        ld      $31,56($sp)
        ld      $fp,48($sp)
        ld      $28,40($sp)
        daddiu  $sp,$sp,64
        j       $31
        nop
