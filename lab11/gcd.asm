	.globl main 

	.text 		

main:
	# Register map
	# $s0 = n1 first random number
	# $s1 = n2 second random number
	# $s2 = i
	# $s3 = gcd value
		
	li $s2, 0 # i=0
	
	# begin for loop (i=0, i<10;i++)
	# n1=random_in_range(1,10000);
	# n2=random_in_range(1,10000);
	# printf("\n G.C.D of %u and %u is %u.", n1, n2, gcd(n1,n2));
	
for:	bge $s2, 9, done # checks for i>=9, negate logic for i<10
	
	li $a0, 1
	li $a1, 10000
	jal random_range # does random_in_range(1,10000)
	move $s0, $v0 # return value and store into $s0 = n1
	
	li $a0, 1
	li $a1, 10000
	jal random_range
	move $s1, $v0 # return value and store in $s1 = n2
	
	# printf("\n G.C.D of %u and %u is %u.", n1, n2, gcd(n1,n2));
	li $v0, 4 # print string
	la $a0, msg
	syscall
	
	li $v0, 1 # 1 to print out integer
	move $a0, $s0
	syscall
	
	li $v0, 4 # prints and
	la $a0, andmsg
	syscall
	
	li $v0, 1 # prints out n2
	move $a0, $s1
	syscall
	
	li $v0, 4 # prints a space
	la $a0, ismsg
	syscall
	
	# move n1 and n2 into arugment registers
	# $a0 has n1
	# $a1 has n2
	move $a0, $s0
	move $a1, $s1
	
	jal gcd # call the gcd function
	move $s3, $v0 # returns gcd value and stores it into $s3 register
	
	li $v0, 1
	move $a0, $s3
	syscall
	
	li $v0, 4
	la $a0, period
	syscall
	
	addi $s2, $s2, 1 # does i++
	j for

#------------------random_in_range(uint32_t low, uint32_t high) function-----
# uint32_t range = high-low+1;
# uint32_t rand_num = get_random();
# return (rand_num % range) + low;

random_range:
	
	# $a0 = high
	# $a1 = low
	# $t0 = range
	# $t1 = random number
	
	# Push $ra and $s0 onto the stack
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	addi $sp, $sp, -4
	sw $s0, 0($sp)
	
	sub $s0, $a1, $a0 # does high-low
	addi $s0, $s0, 1 # high-low + 1
	
	# DON"T PUT TEMPORARY REGISTERS BEFORE CALLING FUNCTIONS
	
	jal random # gets random number
	move $t1, $v0 # stores random number into $t1
	
	divu $t1, $s0 # does modulo division (rand_num % range)
	mfhi $t1 
	addu $t2, $t1, $a0 # adds low
	
	move $v0, $t2 # return result stored in $v0
	
	# Pop $ra and $s0 from the stack
	lw $s0, 0($sp)
	addi $sp, $sp, 4
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	
	jr $ra


#----------------------------------------------------------------------------

#------------------- get_random() function ------------------------

# uint32_t result;
# m_z = 36969 * (m_z & 65535) + (m_z >> 16);
# m_w = 18000 * (m_w & 65535) + (m_w >> 16);
# result = (m_z << 16) + m_w;  /* 32-bit result */
# return result;

random:

	# $t0 = 36969, constant from get_random function
	# $t1 = 18000, constant from get_random function
	# $t2 = 65535, constant from get_random function

	# Push S registers onto the stack
	addi $sp, $sp, -4
	sw $s3, 0($sp) # push $s2 onto stack
	addi $sp, $sp, -4
	sw $s4, 0($sp) # push $s3 onto stack
	addi $sp, $sp, -4
	sw $ra, 0($sp) # push the $ra register onto stack
	
	li $t0, 36969 # t1 = 36969
	li $t1, 18000 # t2 = 18000
	li $t2, 65535 # t3 = 65535
	
	lw $s3,Z # loads global variable Z into s2
	lw $s4,W # loads global variable W into s3
	
	and $t3, $s3, $t2 # does (m_z & 65535)
	mul $t3, $t0, $t3 # does 36969 * (m_z & 65535)
	srl $t4, $s3, 16 # does m_z >> 16
	addu $s3, $t3, $t4 # m_z = 36969 * (m_z & 65535) + (m_z >> 16)
	sw $s3, Z # save global Z
	
	and $t3, $s4, $t2 # does (m_w & 65535)
	mul $t3, $t1, $t3 # does 18000 * (m_w & 65535)
	srl $t4, $s4, 16 # does m_w >> 16
	addu $s4, $t3, $t4 # m_w = 18000 * (m_w & 65535) + (m_w >> 16)
	sw $s4, W # save global W
	
	sll $t3, $s3, 16 # Z<<16
	addu $t3, $t3, $s4 # (Z<<16) + W
	
	# Return result stored in $v0
	move $v0, $t3
	
	# Pop S registers from the stack
	lw $ra, 0($sp) # pop $ra from the stack
	addi $sp, $sp, 4
	lw $s4, 0($sp) # pop $s3 from the stack
	addi $sp, $sp, 4
	lw $s3, 0($sp) # pop $s2 from the stack
	addi $sp, $sp, 4
	
	# Return from fucntion
	jr $ra

#------------------------------------------------------------------
	
#----------------------gcd(uint32_t n1, uint32_t n2)------------------
# uint32_t gcd(uint32_t n1, uint32_t n2)
#    {
#        if (n2 != 0)
#           return gcd(n2, n1%n2);
#        else 
#           return n1;
#    }

gcd:

	# Pushing $ra, $s0, $s1 onto stack
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	addi $sp, $sp, -4
	sw $s0, 0($sp)
	addi $sp, $sp, -4
	sw $s1, 0($sp)
	
	# Move arugment registers into s registers
	# $s0 = $a0 = n1
	# $s1 = $a1 = n2
	move $s0, $a0
	move $s1, $a1
	
	bne $s1, 0, if # checks for (n2 != 0)
	j else # if condition isn't met, then it goes to the else statement
	
if:	# calculate n1%n2
	divu $s0, $s1 # does modulo division (n1%n2)
	mfhi $s0 # remainder is stored in high which is stored in $s0
	
	# Update arguments so we can call gcd again with new arguments
	move $a0, $s1 # moves n2 into $a0
	move $a1, $s0 # moves n1%n2 into $a1
	
	j gcd # call gcd again recursively

else:
	# return n1
	move $v0, $s0
	
	# Popping $ra, $s0, $s1 from the stack
	lw $s1, 0($sp)
	addi $sp, $sp, 4
	lw $s0, 0($sp)
	addi $sp, $sp, 4
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	
	# Return function call
	j $ra

#--------------------------------------------------------------------
	
done:
	
	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	.data
	#assigning variables in memory
	W:	.word 50000
	Z:	.word 60000
	
	msg:		.asciiz "\n G.C.D of "
	andmsg:	.asciiz " and "
	ismsg:		.asciiz " is "
	period:	.asciiz "."
	

