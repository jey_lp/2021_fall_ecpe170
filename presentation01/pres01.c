// Jeyrik Paduga j_paduga@u.pacific.edu
#include <stdio.h>
#include <stdlib.h>

// A linked list node
struct Node
{
	int data; //leaking memory
	struct Node *next;
};

/* Given a reference (pointer to pointer) to the head of a list
and an int inserts a new node on the front of the list. */
void push(struct Node** head_ref, int new_data)
{
	struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));
	new_node->data = new_data;
	new_node->next = (*head_ref);
	(*head_ref) = new_node;
}

/* Given a reference (pointer to pointer) to the head of a list
and a position, deletes the node at the given position */
void deleteNode(struct Node **head_ref, int position)
{
// If linked list is empty
if (*head_ref == NULL)
	return;

// Store head node
struct Node* temp = *head_ref;

	// If head needs to be removed
	if (position == 0)
	{
		*head_ref = temp->next; // Change head
		free(temp);			 // free old head
		 		//seg fault
		return;
	}

	// Find previous node of the node to be deleted
	for (int i=0; temp!=NULL && i<position-1; i++)
		temp = temp->next;

	// If position is more than number of nodes
	if (temp == NULL || temp->next == NULL)
		return;

	// Node temp->next is the node to be deleted
	// Store pointer to the next of node to be deleted
	struct Node *next = temp->next->next;

	// Unlink the node from linked list
	free(temp->next); // Free memory

	temp->next = next; // Unlink the deleted node from list
}

// This function prints contents of linked list starting from
// the given node
void printList(struct Node *node)
{
	while (node != NULL)
	{
		printf(" %d ", node->data);
		node = node->next;
	}
}

/* Driver program to test above functions*/
int main()
{
	printf("Pacific ID Number is 989349031\n");
	printf("After dividing the number by 3, I will get the remainder of 0\n");
	/* Start with the empty list */
	struct Node* head = NULL;

	push(&head, 1);
	push(&head, 3);
	push(&head, 0);
	push(&head, 9);
	push(&head, 4);
	push(&head, 3);
	push(&head, 9);
	push(&head, 8);
	push(&head, 9);

	puts("Created Linked List: ");
	printList(head);
	deleteNode(&head, 4);
	puts("\nLinked List after deleting the 5th integer: ");
	printList(head);
	printf("\n");
	return 0;
}


