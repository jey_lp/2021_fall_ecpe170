# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
main:
	# Map:
	# $s0 = Z
	# $s1 = i
	
	lw $s0, Z #loads Z into $s0
	li $s1, 0
	lw $s1, i #loads i into $s1
	
	# begin first while loop
start:	bgt $s1, 20, if #compare i>20
	addi $s0, $s0, 1 #does Z++
	addi $s1, $s1, 2 #does i+=2
	j start
if:	j do

	# start of the do while loop with Z++
do:	addi $s0, $s0, 1 #does Z++
while:	blt $s0, 100, do #compares Z<100
	j start1 #jumps to next while loop
	
	# start of 3rd while loop
start1: bgt $s1, 0, oper # compares i>0, does the decrementation in oper
	j exit
oper:	sub $s0, $s0, 1 # does Z--
	sub $s1, $s1, 1 # does i--
	j start1
exit:
	sw $s0, Z
	
	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data

	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
	
	# assigning variables Z and i as integer words in memory
	Z:		.word 2
	i:		.word 0
