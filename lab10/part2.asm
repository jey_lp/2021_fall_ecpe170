# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
main:
	#Map:
	#$s0=A
	#$s1=B
	#$s2=C
	#$s3=Z
	
	lw $s0, A #loads A
	lw $s1, B #loads B
	lw $s2, C #loads C
	lw $s3, Z #loads Z
	
	bgt $s0, $s1, if #does A>B
	blt $s2, 5, if #checks for C < 5
	j elif #if the conditions aren't met, then it'll jump to the elif part

if:	li $s3, 1 #loads Z=1
	j done

elif:	bgt $s0, $s1, elif2 #checks for A>B
	j else

elif2:	add $s4, $s2, 1 #does C+1 and saves it into register $t4
	beq $s4, 7, if2 #checks if C+1 = 7, then jumps to if2
	j else
	
if2:	li $s3, 2 #loads Z=2
	j done
	
else:	li $s3, 3 #loads Z=3
	j done

#switch branch begins on Z "switch(Z)"
done:	beq $s3, 1, case1 #checks for case 1: Z=1
	beq $s3, 2, case2 #checks for case 2: Z=2
	li $s3, 0 #this is the default case, loads Z=0
	j exit

case1:	li $s3, -1 #loads Z=-1
	j exit
	
case2:	sub $s3, $s3, -2 #decrements Z by -2 (Z -= -2)
	j exit
	
exit:
	sw $s3, Z #stores final Z into $s3
	
	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data

	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
	
	#assigning values to A,B,C,Z in memory
	A:		.word 10
	B:		.word 15
	C:		.word 6
	Z:		.word 0
