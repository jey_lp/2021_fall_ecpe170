	.globl main 

	.text 		

main:
	# Register map
	# $s0 = starting address of array
	# $s1 = starting address of result
	# $t0 = i
	# $t1 = string[i] address
	# $t2 = string[i] value
	
	# will ask for user input and save it into string or a0 and a1
	la $a0, string # load starting address
	li $a1, 256 #max number of chars
	li $v0, 8
	syscall
	
	li $t0, 0 # i=0
	la $s0, string #address of string
	la $s1, result #address of result
	
	# does while(string[i] != '\0')
while:	
	add $t1, $s0, $t0 # gets string[i] address
	lb $t2, 0($t1)	# $t2 is string[i] value
	beq $t2, 0, finish # negate logic, while string[i]=='\0'
	beq $t2, 'e', if # negate logic, if string[i] != 'e'
	addi $t0, $t0, 1 # i++
	j while
	
if:	# if statement for when string[i] == 'e', so a match
	# combine the if statements with the matching print statements
	# since automatically result with not be NULL, we don't need more
	# lines of code

	# result = &string[i]; 
	sw $t1, result # saves result into $t1
	
	# does printf("First match at address %d\n", result);
	li $v0, 4 # print out string message
	la $a0, addressMsg
	syscall
	
	li $v0, 1 # 1 to print out integer
	la $a0, result # load in the result address
	syscall
	
	# prints a newline
	li $v0, 4
	la $a0, newline
	syscall
	
	# does printf("The matching character is %c\n", *result);
	li $v0, 4 # print_string syscall code = 4
	la $a0, charMsg # load address of msg1
	syscall
	
	li $v0, 11 # print_character syscall code = 11
	la $a0, 0($t2) # load value of string[i]
	syscall
	
	# prints new line
	li $v0, 4
	la $a0, newline
	syscall
	
	j done
	
finish: # this is the else statement
	# does printf("No match found\n");
	li $v0, 4
	la $a0, failMsg
	syscall
	
done:
	
	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	.data
	# assign variables
	string:	.space 256 # this represents the char string[256] array
	
	# printf("First match at address %d\n", result); asciiz form below
	addressMsg:	.asciiz "First match at address "
	
	# printf("The matching character is %c\n", *result); asciiz string below
	charMsg:	.asciiz "The matching character is "
	
	# printf("No match found\n"); string below
	failMsg:	.asciiz "No match found"
	
	# string for newline
	newline:	.asciiz      "\n"
	
	# variable for result
	result:	.space 20
