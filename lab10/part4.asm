# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
main:
	# Map:
	# $t0 = i
	# $s0 = starting address of array
	# $s1 = starting address of array
	# $s2 = C
	
	la $s0, array_A # load starting address to arrayA
	la $s1, array_B # load starting address to arrayB
	lw $s2, C
	
	# for loop begins with i=0
	li $t0, 0 
for:	bge $t0, 5, done # checks if i>=5, then goes to done

	# A[i] = B[i] + C
	mul $t2, $t0, 4 # 4*i for byte size
	add $t2, $t2, $s1 # add offset of the array, $s1 is the address of array B
	lw $t3, 0($t2) #load B[i] into $t3
	
	add $t4, $t3, $s2 # does B[i] + C
	
	mul $t2, $t0, 4 # 4*i for byte size
	add $t2, $t2, $s0 # add offset of the array, $s0 is the address of array A
	sw $t4, 0($t2) # store array_A
	
	addi $t0, $t0, 1 # increment part of the for loop (i++)
	j for
	
done:	sub $t0, $t0, 1 # does i--

start:	bge $t0, 0, oper # begin while loop with condition i>=0
	j done1
	
oper:	mul $t2, $t0, 4 # 4*i for byte size
	add $t2, $t2, $s0 # add offset of the array, $s0 is the address of array A
	lw $t3, 0($t2) # load A[i]
	
	mul $t4, $t3, 2
	
	mul $t2, $t0, 4 # 4*i for byte size
	add $t2, $t2, $s0 # add offset of the array, $s0 is the address of array A
	sw $t4, 0($t2) # store array_A
	
	sub $t0, $t0, 1 # i--
	j start
done1:

	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data

	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
	#assign variables in memory
	array_A:	.space 20 #used 20 bc 4 bytes is 1 element and there are 5, so 5*4=20
	array_B:	.word 1 2 3 4 5 #store {1,2,3,4,5} in array B
	C:		.word 12
