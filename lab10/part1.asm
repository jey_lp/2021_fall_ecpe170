# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
main:
	#Register map:
	#$t0=A
	#$t1=B
	#$t3=C
	#$t4=D
	#$t5=F
	#$t6=Z
	#$t7=(A+B)
	#$t8=(C-D)
	#$t9=(E+F)
	#$s0=(A-C)
	#$s1=(A+B) + (C+D)
	#$s2=(E+F) - (A-C)
	
	li $t0, 15 #load A into register t0 with value 15
	li $t1, 10 #load B into register t1 with value 10
	li $t2, 7 #load C into register t2 with value 7
	li $t3, 2 #load D into register t3 with value 2
	li $t4, 18 #load E into register t4 with value 18
	li $t5, -3 #load F into register t5 with value -3
	li $t6, 0 #load Z into reigster t6 with value zero
	
	add $t7, $t0, $t1 #(A+B)
	sub $t8, $t2, $t3 #(C-D)
	add $t9, $t4, $t5 #(E+F)
	sub $s0, $t0, $t2 #(A-C)
	
	add $s1, $t7, $t8 #(A+B) + (C-D)
	sub $s2, $t9, $s0 #(E+F) - (A-C)
	
	add $t6, $s1, $s2 #(A+B) + (C+D) + (E+F) - (A-C)
	
	sw $t6, int #store value of Z in memory, also in register $t6
	
	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data

	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
	int:		.word 12
