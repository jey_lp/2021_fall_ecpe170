#include <stdint.h>
#include <stdio.h>


uint32_t array1[3][5]; //2D array
uint32_t array2[3][5][7]; //3D array

int main() {
  printf("Printing out the memory addresses of 2D array: array[3][5]\n");
  printf("Iterating through each column and then advance to the next row\n");
  printf("Ex: Memory[0] = array[0][0], Memory[1] = array[0][1]...\n");
  int a = 0;
  int b = 0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 5; j++) {
      printf("Memory address of Memory[%d]: %p\n", a, &array1[i][j]); 
      a++;
    }
  }
  
  printf("Printing out the memory addresses of 3D array: array[3][5][7]\n");
  printf("Iterating through k then the columns then advances to the next row\n");
  printf("Ex: Memory[0] = array[0][0][0], Memory[1] = array[0][0][1]...\n");
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 5; j++) {
      for (int k = 0; k < 7; k++) {
        printf("Memory address of Memory[%d]: %p\n", b, &array2[i][j][k]); 
        b++;      
      }
    }
  }
}
